// community-cards-section.js

/**
 * Dynamically updates the community section with member cards.
 * Each member's information is displayed within a card, including their avatar,
 * name, and position.
 *
 * @param {Object[]} members - Array of member objects to be displayed.
 */
function updateCommunitySection(members) {
  const container = document.querySelector(".app-section__cards--community");
  if (!container) {
    return; // Early exit if the container is not found
  }

  container.innerHTML = ""; // Clear any existing content

  members.forEach((member) => {
    const cardHTML = `
      <div class="app-section__cards__card">
        <img src="${member.avatar}" alt="${member.firstName} ${member.lastName}" width="150" height="150" />
        <p class="app-section__cards__description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.
        </p>
        <p class="app-section__cards__title">${member.firstName} ${member.lastName}</p>
        <p class="app-section__cards__position">${member.position}</p>
      </div>
    `;

    container.innerHTML += cardHTML;
    // let div = document.createElement("div");
    // div.innerHTML = cardHTML;
    // container.append(div);
  });
}

/**
 * Fetches community member data from the server and updates the community section
 * by calling `updateCommunitySection`. Handles any errors that occur during the fetch operation.
 */
function fetchCommunityData() {
  fetch("http://localhost:3000/community")
    .then((response) => response.json())
    .then((data) => updateCommunitySection(data))
    .catch((error) => console.error("Fetch error:", error));
}

/**
 * An object to initialize the community cards section of the application.
 *
 * @type {{init: Function}}
 */
export const CommunitySection = {
  init: fetchCommunityData,
};
