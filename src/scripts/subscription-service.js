// subscription-service.js

/**
 * Sends a POST request to the server with the user's email.
 *
 * @param {string} email - The email address to subscribe.
 * @returns {Promise<Response>} The response from the server as a promise.
 */
function subscribeUser(email) {
  // Define the endpoint URL.
  const url = "http://localhost:3000/subscribe";

  // Create the fetch options for the POST request.
  const fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email: email }),
  };

  // Send the POST request using fetch and return the response promise.
  return fetch(url, fetchOptions)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .catch((error) => {
      console.error(
        "There has been a problem with your fetch operation:",
        error
      );
      throw error; // Re-throw the error for further handling.
    });
}

export { subscribeUser };
